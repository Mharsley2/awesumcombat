class Player {
    constructor(hp, strength, defense, hpElement) {
        this.hp = hp;
        this.strength = strength;
        this.defense = defense;
        this.hpElement = hpElement;
        this.potion = 3;
    }
    attack(enemy) {
        enemy.hp -= this.strength - enemy.defense;
        enemy.updateHealth();
    }
    updateHealth() {
        this.hpElement.style.width = this.hp + '%';
    }
    usePotion() {
        if (this.potion > 0) {
            this.hp += 10;
            this.updateHealth();
        }
        this.potion -= 1;
    }
}


const playerOne = new Player(100, 20, 5, document.getElementById('player-hp'))
const playerTwo = new Player(100, 17, 8, document.getElementById('enemy-hp'))